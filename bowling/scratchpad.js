function checkScore(){
  
  let history = [];
  let line;
  let total;
  let score_for_line;

  function isStrike(){
    return (this.line.length == 1 ? true : false);
  };
  
  function isSpare(){
    return (this.line[0] + this.line[1] == 10 ? true : false);
  };
  
  function pushToHistory(){
    history.push({
      score: this.score_for_line,
      line: this.line,
      isStrike: isStrike(),
      isSpare: isSpare(),
    });
  };
  
  function scoreTheLine(){
   if(isStrike() || isSpare()){
      this.score_for_line = 10;
    }else {
      this.score_for_line = this.line[0] + this.line[1];
    };
  };
//TODO: this incorectly adds the numbers
  function changeHistory(){
 	let lastItem = history.length;
	let i;
	for (i = 0; i < lastItem; i++){
		if(history[i].isStrike && (history[i+2] != undefined )){
			history[i].score += (history[i+1].score + history[i+2].score);
		}else if(history[i].isSpare && (history[i+1] != undefined)){
			history[i].score += history[i+1].score;
		}	
	};		
  }

  function addTotal(){
    let sum = 0;

    history.forEach(function(element){
   	 sum += element.score;
    });
	  
    return sum;
  }

  return function(line){
    this.line = line;
    
    scoreTheLine();
    pushToHistory();
    changeHistory(); 
    return addTotal();
  };
}; 

var line1 = [4,6];
var line2 = [10];
var line3 = [1,7];
var line4 = [1,2];

score = checkScore();

score(line1);
score(line2);
score(line3);

total = score(line4);
console.log(total)
